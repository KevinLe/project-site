let android;
let lockScreen;
let screenWidth;
let screenHeight;
let buttonHome;

function initAndroidQ() {
    console.log("android q");
    android = $('.android-q');
    lockScreen = android.find('.lock-screen');
    screenWidth = android.width();
    screenHeight = android.height();

    android.click(function (event) {
        //event.stopPropagation();
        console.log("android q click");
    });

    setHomeButtonEvent();
    screenTouchEvent();
    initDateAndTime();

    function setHomeButtonEvent() {
        buttonHome = $('.home-button');

        buttonHome.mousedown(function (event) {
            console.log("home button mousedown");
            buttonHome.addClass('click');
        });
        buttonHome.mouseup(function (event) {
            console.log("home button mouseup");
            buttonHome.removeClass('click');
        });
    }

    function initDateAndTime() {

        let currentDate = new Date();

        let timeView = android.find('.lock-screen .info .time');
        let timeStr = moment(currentDate).format('HH:mm');
        timeView.html(timeStr);

        let dateView = android.find('.screen-lock .info .date');
        let dateStr = moment(currentDate).format('ddd, MMM D');
        dateView.html(dateStr);

        function addZero(num) {
            return num < 10 ? '0' + num : num;
        }
    }
}

function screenTouchEvent(callback) {
    let touchStart, touchEnd;
    android.on('touchstart mousedown', function (event) {
        let touch = event.type == 'touchstart' ? event.originalEvent.targetTouches[0] : event;
        touchStart = new Touch(touch.pageX, touch.pageY);
        console.log("touchstart x: " + touchStart.x + ", y: " + touchStart.y);
    });

    android.on('touchend mouseup', function (event) {
        let touch = event.type == 'touchend' ? event.originalEvent.changedTouches[0] : event;
        touchEnd = new Touch(touch.pageX, touch.pageY);
        console.log("touchend x: " + touchEnd.x + ", y: " + touchEnd.y);
        touchGesture();
    });

    function touchGesture() {
        if (touchStart.y - touchEnd.y > screenHeight * 0.2) {
            if (typeof callback === 'function') {
                callback();
                unlock();
            }
        }
    }
}

function expandApps(){
    let apps = android.find('.apps');
    apps.animate({
        marginTop: "0%"
    });
    buttonHome.addClass('dark');
}

function collapseApps(){
    let apps = android.find('.apps');
    apps.animate({
        marginTop: "210%"
    });
    buttonHome.removeClass('dark');
}

function unlock() {
    lockScreen.animate({
        opacity: "0"
    });
    buttonHome.css({
        opacity: "1"
    });
    expandApps();
}

var androidLock = function () {
    lockScreen.animate({
        opacity: "1"
    });
    buttonHome.css({
        opacity: "0"
    });
    collapseApps();
}

var App = function (name) {
    initView();

    function initView() {
        let container = android.find('.apps .container');
        container.append(getHtml());

        function getHtml() {
            let html = '<div class="app">';
            html += '<div class="icon">';
            html += getAppIconHtml();
            html += '</div>';
            html += '<div class="title">';
            html += name;
            html += '</div>';
            html += '</div>';
            return html;

            function getAppIconHtml() {
                let html = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="20 20 68 68">';
                html += '<path fill="#008577" d="M0,0h108v108h-108z" />';
                html += '<path fill="#00000000" d="M9,0L9,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M19,0L19,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M29,0L29,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M39,0L39,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M49,0L49,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M59,0L59,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M69,0L69,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M79,0L79,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M89,0L89,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M99,0L99,108" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,9L108,9" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,19L108,19" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,29L108,29" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,39L108,39" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,49L108,49" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,59L108,59" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,69L108,69" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,79L108,79" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,89L108,89" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M0,99L108,99" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M19,29L89,29" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M19,39L89,39" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M19,49L89,49" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M19,59L89,59" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M19,69L89,69" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M19,79L89,79" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M29,19L29,89" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M39,19L39,89" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M49,19L49,89" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M59,19L59,89" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M69,19L69,89" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#00000000" d="M79,19L79,89" stroke-width="0.8" stroke="#33FFFFFF" />';
                html += '<path fill="#000000" opacity="0.3" d="M32,64C32,64 38.39,52.99 44.13,50.95C51.37,48.37 70.14,49.57 70.14,49.57L108.26,87.69L108,109.01L75.97,107.97L32,64Z" />';
                html += '<path fill="#FFFFFF" d="M66.94,46.02L66.94,46.02C72.44,50.07 76,56.61 76,64L32,64C32,56.61 35.56,50.11 40.98,46.06L36.18,41.19C35.45,40.45 35.45,39.3 36.18,38.56C36.91,37.81 38.05,37.81 38.78,38.56L44.25,44.05C47.18,42.57 50.48,41.71 54,41.71C57.48,41.71 60.78,42.57 63.68,44.05L69.11,38.56C69.84,37.81 70.98,37.81 71.71,38.56C72.44,39.3 72.44,40.45 71.71,41.19L66.94,46.02ZM62.94,56.92C64.08,56.92 65,56.01 65,54.88C65,53.76 64.08,52.85 62.94,52.85C61.8,52.85 60.88,53.76 60.88,54.88C60.88,56.01 61.8,56.92 62.94,56.92ZM45.06,56.92C46.2,56.92 47.13,56.01 47.13,54.88C47.13,53.76 46.2,52.85 45.06,52.85C43.92,52.85 43,53.76 43,54.88C43,56.01 43.92,56.92 45.06,56.92Z" />';
                html += '</svg>';
                return html;
            }
        }
    }
}

var Touch = function (x, y) {
    return {
        x: x,
        y: y
    };
};
