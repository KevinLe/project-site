let pixel3XL;
let backButtonClickListener = null;

function initPixel3XL() {
    pixel3XL = $('.pixel3-xl');

    setWindowEvent();

    function setWindowEvent() {
        scale();
        $(window).resize(function () {
            scale();
        });
    }
}

function showBackButton() {
    let backButton = $('.back-button');
    backButton.addClass('show');
    backButton.click(function (event) {
        backButton.removeClass('show');
        scale();
        if(backButtonClickListener != null){
            backButtonClickListener();
        }
    })
}

function scale(fullScreen = false) {
    let deviceHeight = $('.pixel3-xl').height();
    let deviceWidth = $('.pixel3-xl').width();
    let windowHeight = $(window).height();
    let windowWidth = $(window).width();
    let rate = (windowHeight * (fullScreen ? 1 : 0.6)) / deviceHeight;
    let top = windowHeight * 0.5 - deviceHeight / 2 * rate;
    let left = windowWidth * 0.5 - deviceWidth / 2 * rate;

    if (fullScreen) {
        pixel3XL.css({
            "transition": 'all .5s'
        });
    }

    pixel3XL.css({
        "transform": 'scale(' + rate + ')',
        "left": left,
        "top": top,
        opacity: 1
    });
}

function setBackButtonClickListener(listener) {    
    if (typeof listener === 'function') {
        backButtonClickListener = listener;
    }
}

var fullScreen = function () {
    showBackButton();
    scale(true);
}
